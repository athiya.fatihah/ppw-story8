from django import forms

class SearchForm(forms.Form):
    attrs = {
            'type':'text',
            'class': 'form-control',
            'id':'search-input',
            'placeholder':'Type a book title or author'
            }

    text_input = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=attrs))
